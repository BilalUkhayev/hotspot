package kz.bilal.hotspot.service;

import kz.bilal.hotspot.model.User;
import kz.bilal.hotspot.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service

public class UserServiceImpl implements IUserService {
    @Autowired
    UserRepository userRepository;

    @Override
    public User getById(Long id) {
        log.info("IN CustomerServiceImpl getById",id);
        return userRepository.getOne(id);
    }

    @Override
    public void save(User user) {
        log.info("IN CustomerServiceImpl save",user);
        userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        log.info("IN CustomerServiceImpl delete",id);
        userRepository.deleteById(id);
    }

    @Override
    public List<User> getAll() {
        log.info("IN CustomerServiceImpl getAll");

        return userRepository.findAll();
    }
}
