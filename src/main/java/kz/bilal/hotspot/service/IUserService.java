package kz.bilal.hotspot.service;

import kz.bilal.hotspot.model.User;

import java.util.List;

public interface IUserService {
    User getById(Long id);
    void save(User user);
    void delete(Long id);
    List<User> getAll();
}
