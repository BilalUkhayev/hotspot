package kz.bilal.hotspot.repository;

import kz.bilal.hotspot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
